const footer = document.querySelector('footer');
const newLink = document.createElement('a');
newLink.textContent = 'Learn More';
newLink.setAttribute('href', '#'); 

footer.appendChild(newLink);



const main = document.querySelector('main');
const featuresSection = document.querySelector('.features'); // Припускаючи, що секція "Features" має клас "features".
const select = document.createElement('select');
select.id = 'rating';

main.insertBefore(select, featuresSection);

const optionsStar = [
    { value: '4', text: '4 Stars' },
    { value: '3', text: '3 Stars' },
    { value: '2', text: '2 Stars' },
    { value: '1', text: '1 Star' }
];

optionsStar.forEach(optionStar => {
    const option = document.createElement('option');
    option.value = optionStar.value;
    option.textContent = optionStar.text;
    select.appendChild(option);
});
